package sdv.cicd.tp.listing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


public class ListingApplication {

	void compter() throws InterruptedException {
		for (int t = 0; t < 10; t++) {
			Thread.sleep(1000);
			System.out.println(t + " s");
		}

	}

	public static void main(String[] args) {
		try {
			System.out.println("Fichiers du répertoire courant");
			System.out.println("=============================");
			new ListingApplication().compter();
			System.out.println("=============================");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
