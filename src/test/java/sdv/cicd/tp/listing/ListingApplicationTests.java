package sdv.cicd.tp.listing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;



class ListingApplicationTests {

	static ListingApplication listingApplication;

	@BeforeAll
	static void setup() {
		listingApplication = new ListingApplication();
	}

	@Test
	void contextLoads() throws InterruptedException {
		System.out.println("Début du test");
		long start = System.currentTimeMillis() / 1000;
		listingApplication.compter();
		long end = System.currentTimeMillis() / 1000;
		Assertions.assertEquals( start + 10, end);
		System.out.println("Fin du test");
	}

}
